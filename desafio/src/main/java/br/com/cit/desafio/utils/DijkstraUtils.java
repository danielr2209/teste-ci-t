package br.com.cit.desafio.utils;

import java.util.List;

import br.com.cit.desafio.dijkstra.Edge;
import br.com.cit.desafio.dijkstra.Vertex;

public class DijkstraUtils {

	public static Vertex getVertex(List<Vertex> list, String id) {
		for(Vertex v : list) {
			if(v.getId().equals(id)) {
				return v;
			}
		}
		return null;
	}
	
	public static Edge getEdge(List<Edge> list, String originId, String destinationId) {
		for(Edge e : list) {
			if(e.getSource().getId().equals(originId) &&
					e.getDestination().getId().equals(destinationId)) {
				return e;
			}
		}
		return null;
	}
	
	public static Float getCost(List<Edge> list, Float value, List<Vertex> vertexes) {
		Float cost = 0.0f;
		for(int index = 0; index < vertexes.size() - 1; index++) {
			cost += getEdge(list, vertexes.get(index).getId(), vertexes.get(index+1).getId()).getWeight();
		}
		return cost * value;
	}
}
