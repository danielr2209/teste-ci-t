package br.com.cit.desafio.dijkstra;

public class Edge  {
    private final Vertex source;
    private final Vertex destination;
    private final float weight;

    public Edge(Vertex source, Vertex destination, float weight) {
        this.source = source;
        this.destination = destination;
        this.weight = weight;
    }

    public Vertex getDestination() {
        return destination;
    }

    public Vertex getSource() {
        return source;
    }
    public float getWeight() {
        return weight;
    }

    @Override
    public String toString() {
        return source + " " + destination;
    }

}
