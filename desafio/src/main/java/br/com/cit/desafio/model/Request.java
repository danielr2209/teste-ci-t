package br.com.cit.desafio.model;

public class Request {
	
	private String origem;
	private String destino;
	private float autonomia;
	private float valor;
	
	public Request(String origem, String destino, float autonomia, float valor) {
		this.setOrigem(origem);
		this.setDestino(destino);
		this.setAutonomia(autonomia);
		this.setValor(valor);
	}

	public String getOrigin() {
		return origem;
	}

	public void setOrigem(String origin) {
		this.origem = origin;
	}

	public String getDestination() {
		return destino;
	}

	public void setDestino(String destination) {
		this.destino = destination;
	}

	public float getAutonomia() {
		return autonomia;
	}

	public void setAutonomia(float autonomia) {
		this.autonomia = autonomia;
	}

	public float getValor() {
		return valor;
	}

	public void setValor(float valor) {
		this.valor = valor;
	}

}
