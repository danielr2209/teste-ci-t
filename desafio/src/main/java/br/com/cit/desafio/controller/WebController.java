package br.com.cit.desafio.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;

import br.com.cit.desafio.dijkstra.DijkstraAlgorithm;
import br.com.cit.desafio.dijkstra.Edge;
import br.com.cit.desafio.dijkstra.Graph;
import br.com.cit.desafio.dijkstra.Vertex;
import br.com.cit.desafio.model.Request;
import br.com.cit.desafio.model.Response;
import br.com.cit.desafio.model.Route;
import br.com.cit.desafio.utils.DijkstraUtils;

@RestController
public class WebController {

	@RequestMapping(value = "/getRoutes", method = RequestMethod.GET)
	public String getRoute() throws FileNotFoundException {
		String response = new String();
		Gson gson = new Gson();
		JsonReader jsonReader = new JsonReader(new FileReader("./resource/routes"));
		List<Route> routes = gson.fromJson(jsonReader, new TypeToken<List<Route>>() {}.getType());
		if(routes != null && !routes.isEmpty()) {
			for(Route route : routes) {
				response += route.getOrigin() + route.getDestination() + " " +
						route.getDistance() + '\n';
			}
		}
		return response;
	}
	
	@RequestMapping(value = "/addRoute", method = RequestMethod.POST)
	public List<Route> addRoute(@RequestBody Route input) throws IOException {
		Gson gson = new Gson();
		File tmpFile = new File("./resource/routes");
		if(!tmpFile.exists()) {
			tmpFile.createNewFile();
		}
		JsonReader jsonReader = new JsonReader(new FileReader("./resource/routes"));
		List<Route> routes = gson.fromJson(jsonReader, new TypeToken<List<Route>>() {}.getType());
		if(routes == null) {
			routes = new ArrayList<Route>();
		}
		routes.add(input);
		FileWriter fw = new FileWriter("./resource/routes");
		fw.write(gson.toJson(routes));
		fw.close();
		return routes;
	}
	
	@RequestMapping(value = "/clearRoutes", method = RequestMethod.DELETE)
	public String clearRoutes() throws FileNotFoundException {
		new PrintWriter("./resource/routes").close();
		return "Rotas apagadas";
	}
	
	@RequestMapping(value = "/request", method = RequestMethod.GET)
	public Response getShortestPath(@RequestBody Request req) throws FileNotFoundException {
		Gson gson = new Gson();
		Graph graph = new Graph();
		File tmpFile = new File("./resource/routes");
		if(!tmpFile.exists()) {
			return null;
		}
		JsonReader jsonReader = new JsonReader(new FileReader("./resource/routes"));
		List<Route> routes = gson.fromJson(jsonReader, new TypeToken<List<Route>>() {}.getType());
		if(routes != null && !routes.isEmpty()) {
			HashSet<String> vertexSet = new HashSet<String>();
			List<Vertex> vertexes = new ArrayList<Vertex>();
			List<Edge> edges = new ArrayList<Edge>();
			for(Route route : routes) {
				if(!vertexSet.contains(route.getOrigin())){
					vertexes.add(new Vertex(route.getOrigin()));
					vertexSet.add(route.getOrigin());
				}
				if(!vertexSet.contains(route.getDestination())){
					vertexes.add(new Vertex(route.getDestination()));
					vertexSet.add(route.getDestination());
				}
				edges.add(new Edge(DijkstraUtils.getVertex(vertexes, route.getOrigin()),
						DijkstraUtils.getVertex(vertexes, route.getDestination()), route.getDistance()));
			}
			graph.setVertexes(vertexes);
			graph.setEdges(edges);
			DijkstraAlgorithm dijkstra = new DijkstraAlgorithm(graph);
			dijkstra.execute(DijkstraUtils.getVertex(vertexes, req.getOrigin()));
			vertexes = dijkstra.getPath(DijkstraUtils.getVertex(vertexes, req.getDestination()));
			
			Response resp = new Response();
			String rota = new String();
			for(Vertex v : vertexes) {
				rota += v.getId() + " ";
			}
			resp.setRota(rota);
			resp.setCusto(DijkstraUtils.getCost(edges, req.getValor(), vertexes));
			return resp;
		}
		return null;
	}
	
}
