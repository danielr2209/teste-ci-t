package br.com.cit.desafio.model;

public class Route {
	
	private String origem;
	private String destino;
	private float distancia;
	
	public Route(String origem, String destino, float distancia) {
		this.setOrigem(origem);
		this.setDestino(destino);
		this.setDistancia(distancia);
	}

	public String getOrigin() {
		return origem;
	}

	public void setOrigem(String origem) {
		this.origem = origem;
	}

	public String getDestination() {
		return destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	public float getDistance() {
		return distancia;
	}

	public void setDistancia(float distance) {
		this.distancia = distance;
	}
	
}
