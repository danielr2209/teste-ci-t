package br.com.cit.desafio.model;

public class Response {

	private String rota;
	private float custo;
	
	public Response() {}
	
	public Response(String rota, float custo) {
		this.setRota(rota);
		this.setCusto(custo);
	}

	public String getRota() {
		return rota;
	}

	public void setRota(String rota) {
		this.rota = rota;
	}

	public float getCusto() {
		return custo;
	}

	public void setCusto(float custo) {
		this.custo = custo;
	}

	
}
