# Teste CI-T

Desafio CI&T - processo seletivo

Para executar o projeto, importe-o no eclipse: *File > Import > Maven > Existing Maven Projects*

Clique em next e selecione a pasta do projeto ("desafio").

Execute DesafioApplication.java e acesse pelo navegador: http://localhost:8080/getRoutes e verifique
se a malha logística no exemplo do enunciado é mostrada, para confirmar o funcionamento.

Usando o Postman, execute os seguintes passos:

- Para essas rotas, confirme o menor custo de A até D, conforme o enunciado:

[GET] http://localhost:8080/request

```
{  
    "origem":"A",
    "destino":"D",
    "autonomia":"10",
    "valor":"2.50"
}
```


```
{
    "rota": "A B D ",
    "custo": 62.5
}
```
Obs: acredito que o valor 6.25 do enunciado está errado

- Delete as rotas já existentes:

[DELETE] http://localhost:8080/clearRoutes

- Adicione novas rotas, como no exemplo do enunciado:

[POST] http://localhost:8080/addRoute

```
{  
    "origem":"D",
    "destino":"E",
    "distancia":"30"
}
```


- Teste outros menores caminho, dadas as rotas adicionadas, para verificar a corretude